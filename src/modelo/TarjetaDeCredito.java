package modelo;

import java.util.ArrayList;

public class TarjetaDeCredito implements Tarjeta{
	
	ArrayList<String> historial;
	
	TarjetaDeCredito(){
		 historial = new ArrayList<String>();
	}
	
	@Override
	public ArrayList<String> debitar(double monto, String comercio,Cuenta account) {
		account.setSaldo(account.getSaldo()-monto);
		
		historial.add(comercio+" ----------------------------- "+ monto);
		return historial;
	}

}
