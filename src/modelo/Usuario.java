package modelo;

public class Usuario {
	private String nombre;
	private String identificacion;
	
	
	public String getIdentificacion() {
		if(identificacion==null)
		{
			return "no existe una identificacion";
		}
		return identificacion;
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}
	
	
	
	
}
