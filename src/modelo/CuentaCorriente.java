package modelo;

public class CuentaCorriente extends Cuenta{
	
	
	@Override
	public void retirar(double monto) {
		if(!((saldo -= (monto-(monto * 0.001))) <0)) {
			saldo -= (monto-(monto * 0.001));
		}
		
	}
	
	@Override
	public void depositar(double monto) {
		saldo += (monto-(monto * 0.001));
	}
}
