package modelo;

import javax.swing.JOptionPane;

/**
 * This class have methods for a Account
 * @author Abiezer Sifontes
 *
 */
public class Cuenta {
	private String numeroCuenta;
	protected double saldo;
	
	/**
	 * lasjldfjslkajfalsfjdlka
	 * @return tgtlkgjlk
	 */
	public String getNumeroCuenta() {
		return numeroCuenta;
	}
	
	/**
	 * Este metodo permite retornar un tipo de cuenta en funcion de una seleccion del usuraio
	 * @param tipoCuenta 1 CuentaCorriente 2 CuentaAhorro 3 Cuenta
	 * @return Cuenta
	 */
	public static Cuenta obtenerCuenta(String tipoCuenta){
		Cuenta account;
		switch(tipoCuenta) {
		case "1":
			account = new CuentaCorriente();
		break;
		case "2":
			account = new CuentaAhorro();
		break;
		
		default:
			account = new Cuenta();
		break;
		} 
		return account;
	}
	
	/**
	 * Esta funcion permite a�adir un numero de Cuenta
	 * @param numeroCuenta El numero de cuenta a ingresar
	 * @return no retorna nada
	 * 
	 */
	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}
	
	public double getSaldo() {
		
		return saldo;
	}
	
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	
	public void depositar(double monto) {
		this.saldo +=monto;
	}
	
	public void retirar(double monto) {
		if(!((saldo -=monto) <0)) {
			saldo -=monto;	
		}
	}
	
	
	
	public String funcion() {
		return "";
	}
	
	public void opcionesCuenta(Cuenta account,Tarjeta tarj) {
		String continuar = "";
		String cadenaAcum = "";
		
		do {
			String opcion = JOptionPane.showInputDialog("Presione 1 Para Realizar un deposto\n"
					+ "Presione 2 Para Realizar un retiro"
					+ "Presione 3 Para Hacer un Debito");
			switch(opcion) {
			case "1":
				String montoDep = JOptionPane.showInputDialog("Introduzca un monto");
				cadenaAcum += "DEPOSITO \t"+montoDep;
				account.depositar(Double.parseDouble(montoDep));
			break;
			case "2":
				String montoRet = JOptionPane.showInputDialog("introduzca un monto");
				cadenaAcum += "RETIRO \t"+montoRet;
				account.retirar(Double.parseDouble(montoRet));
			break;
			case "3":
				String montoDeb = JOptionPane.showInputDialog("introduzca un monto");
				cadenaAcum += "RETIRO \t"+montoDeb;
				tarj.debitar(Double.parseDouble(montoDeb),"Comercial XX",account);
			break;
			default:
				JOptionPane.showMessageDialog(null,"no introdujo una opcion valida");
			break;
			}
			 continuar = JOptionPane.showInputDialog("introduzca 1 para finalizar");
		}while(!continuar.equals("1"));
		
		JOptionPane.showMessageDialog(null,cadenaAcum+"\n\n El saldo final es:\n"+account.getSaldo()+"\n");
	}
	
}
