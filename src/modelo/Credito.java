package modelo;

public abstract class Credito<T> {
	protected T numeroCredito;
	
	public T getNumeroCredito() {
		return numeroCredito;
	}

	public void setNumeroCredito(T numeroCredito) {
		this.numeroCredito = numeroCredito;
	}

	public abstract double obtenerCuota(); 
	
	public int seleccionarPlazo(int plazo){
		if(plazo<6) {
			return 6;
		}else if(plazo<12) {
			return 12;
		}else {
			return 36;
		}
	}
}
