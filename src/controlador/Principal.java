package controlador;

import javax.swing.JOptionPane;

import modelo.Credito;
import modelo.CreditoEmpresa;
import modelo.Cuenta;
import modelo.Usuario;

public class Principal extends Cuenta{
		
	public static void main(String[] args) {
		 
		
		String tipoCuenta = JOptionPane.showInputDialog("Presiona 1 para abrir una Cuenta Corriente"
				+ "\nPresiona 2 para abrir una cuenta de ahorro");
		
		Cuenta account = Cuenta.obtenerCuenta(tipoCuenta);
		
		String identificacion = JOptionPane.showInputDialog("Ingrese su identificacion");
		String nombre = JOptionPane.showInputDialog("Ingrese su nombre");
		
		Usuario user = new Usuario();
		user.setIdentificacion(identificacion);
		user.setNombre(nombre);
		
		account.opcionesCuenta(account);
		
		
	}
	
}
